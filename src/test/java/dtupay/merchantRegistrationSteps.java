package dtupay;

import dtupay.model.Merchant;
import dtupay.repositories.MerchantRepository;
import dtupay.service.MerchantUserService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.ArrayList;
import java.util.UUID;

import messaging.Event;
import messaging.MessageQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import lombok.Data;

@Data
public class merchantRegistrationSteps {

    private MessageQueue q = mock(MessageQueue.class);
    private MerchantRepository merchants;
    private MerchantUserService service = new MerchantUserService(merchants, q);
    private Merchant merchant;
    private UUID registeredId;
    private ArrayList<Merchant> expected;

    @Given("there is a merchant that has not been registered")
    public void thereIsAMerchantThatHasNotBeenRegistered() {
        merchant = new Merchant("firstName","lastName", "12345678", "321");
//        assertNull(merchant.getId());
    }

    @When("the {string} event is received")
    public void theEventIsReceived(String e) {
        Event registrationEvent = new Event(e, new Object[] {merchant});
        service.handleMerchantRegistration(registrationEvent);
        registeredId = service.registerMerchant(merchant.getFirstName(), merchant.getLastName(), merchant.getCpr(), merchant.getAccountId());
    }

    @Then("the merchant is registered")
    public void theMerchantIsRegistered() {
//        expected = new ArrayList<>(merchants.getMerchants());
        assertNotNull(registeredId);
    }

    @And("the merchant is stored in the merchant repository")
    public void theMerchantIsStoredInTheMerchantRepository() {
        assertTrue(expected.stream().anyMatch(m -> m.getId().equals(registeredId)));
    }

//    @When("the merchant is being registered")
//    public void theMerchantIsBeingRegistered() {
//        new Thread(()-> {
//            var result = service.register(merchant);
//            registeredMerchants.complete(result);
//        }).start();
//    }
//
//    @Then("the {string} event is sent")
//    public void theEventIsSent(String arg0) {
//        Event event = new Event(arg0, new Object[] { merchant });
//        assertEquals(event,publishedEvent.join());
//    }
//
//    @When("the {string} event is received")
//    public void theEventIsReceived(String arg0) {
//        var c = new Merchant();
//        c.setName(merchant.getName());
//        c.setAccountId("123");
//        service.handleMerchantRegistration(new Event("..",new Object[] {c}));
//    }
//
//    @Then("the merchant is registered")
//    public void theMerchantIsRegisteredAndHisIdIsSet() {
//        assertNotNull(registeredMerchants.join().getAccountId());
//    }
//
//
//    @And("the merchant is stored in the merchant repository")
//    public void theMerchantIsStoredInTheMerchantRepository() {
//    }
}
