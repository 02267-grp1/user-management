package dtupay.model;

import lombok.Data;
/**
    @author s213555
*/
@Data
public class MerchantFacade {
    String firstName, lastName, cpr, bankAccountId;
}
