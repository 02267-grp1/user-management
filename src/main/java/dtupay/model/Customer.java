package dtupay.model;

import lombok.Data;

import java.util.UUID;
/**
    @author s194626
*/
@Data
public class Customer {
    private String firstName, lastName, accountId, cpr;
    private UUID ID;

    public Customer(String firstName, String lastName,String cpr, String bankAccountId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cpr = cpr;
        this.accountId = bankAccountId;
        ID = UUID.randomUUID();
    }
}
