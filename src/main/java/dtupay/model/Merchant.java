package dtupay.model;

import lombok.Data;

import java.io.Serializable;
import java.util.UUID;
/**
    @author s213555
*/
@Data
public class Merchant implements Serializable {
    private String firstName, lastName, cpr, accountId;
    private UUID id;

    public Merchant(String firstName, String lastName, String cpr, String bankAccountId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cpr = cpr;
        this.accountId = bankAccountId;
        id = UUID.randomUUID();
    }

}
