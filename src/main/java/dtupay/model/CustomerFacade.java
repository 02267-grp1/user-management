package dtupay.model;

import lombok.Data;
/**
    @author s202082
*/
@Data
public class CustomerFacade {
    String firstName, lastName, cpr, bankAccountId;
}
