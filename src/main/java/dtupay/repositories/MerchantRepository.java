package dtupay.repositories;

import dtupay.model.Merchant;

import java.util.UUID;

public interface MerchantRepository {
    /**
        @author s213555
    */
    void add(Merchant merchant);
    /**
        @author s213555
    */
    Merchant findMerchant(UUID merchantId);

    /**
     *
     * @author s194626
     */
    void removeMerchant(Merchant merchant);
}
