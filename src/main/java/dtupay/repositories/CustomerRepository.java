package dtupay.repositories;

import dtupay.model.Customer;

import java.util.UUID;

public interface CustomerRepository {
    /**
        @author s194626
    */
    void add(Customer customer);
    /**
        @author s194626
    */
    Customer findCustomer(UUID customerId);
    /**
     * @author s202082
     */
    void removeCustomer(Customer customer);
}
