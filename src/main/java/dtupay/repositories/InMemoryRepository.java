package dtupay.repositories;

import dtupay.model.Customer;
import dtupay.model.Merchant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/** 
    Please look at {@link CustomerRepository} and {@link MerchantRepository} for the author of each method
 */
public class InMemoryRepository implements CustomerRepository, MerchantRepository{

    private final static List<Customer> customers = Collections.synchronizedList(new ArrayList<>());
    private final static List<Merchant> merchants = Collections.synchronizedList(new ArrayList<>());

    @Override
    public void add(Customer customer) {
        customers.add(customer);
    }

    @Override
    public Customer findCustomer(UUID customerId) {
        return customers.stream().filter(c -> c.getID().equals(customerId))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void add(Merchant merchant) {
        merchants.add(merchant);
    }

    @Override
    public Merchant findMerchant(UUID merchantId){
        return merchants.stream().filter(m -> m.getId().equals(merchantId))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeMerchant(Merchant merchant) {
        merchants.remove(merchant);
    }


    @Override
    public void removeCustomer(Customer customer) {
        customers.remove(customer);
    }
}
