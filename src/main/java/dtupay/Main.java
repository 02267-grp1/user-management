package dtupay;

import dtupay.repositories.InMemoryRepository;
import dtupay.service.CustomerUserService;
import dtupay.service.MerchantUserService;
import messaging.implementations.RabbitMqQueue;

public class Main {
    public static void main(String[] args) {
        var mq = new RabbitMqQueue("rabbitmq");
        var database = new InMemoryRepository();
        new CustomerUserService(database, mq);
        new MerchantUserService(database, mq);
    }
}
