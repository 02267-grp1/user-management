package dtupay.service;

import java.util.UUID;

import dtupay.model.Merchant;
import dtupay.model.MerchantFacade;
import lombok.Getter;

import dtupay.repositories.MerchantRepository;
import messaging.Event;
import messaging.MessageQueue;

public class MerchantUserService {

    MessageQueue queue;
    @Getter
    private final MerchantRepository merchants;

    public MerchantUserService(MerchantRepository merchantRepository, MessageQueue q){
        this.queue = q;
        this.merchants = merchantRepository;
        this.queue.addHandler("MerchantRegistrationRequested", this::handleMerchantRegistration);
        this.queue.addHandler("MerchantAccountRequested", this::handleFindMerchantBankAccount);
        this.queue.addHandler("MerchantDeregisterRequested", this::handleMerchantDeregister);
        System.out.println("Merchant User Service Initialized");

    }
    /**
        @author s213555
    */
    public UUID registerMerchant(String firstName, String lastName, String cpr, String accountId){
       Merchant merchant = new Merchant(firstName, lastName, cpr, accountId);
       merchants.add(merchant);
       return merchant.getId();
    }
    /**
        @author s213555
    */
    public String findMerchantBankAccount(UUID merchantId){
        return merchants.findMerchant(merchantId).getAccountId();
    }
    /**
        @author s213555
    */
    public void handleFindMerchantBankAccount(Event event){
        var id = event.getArgument(0, UUID.class);
        var bankAccount = findMerchantBankAccount(id);
        queue.publish(new Event("MerchantAccountResult", new Object[]{bankAccount}));
    }
    /**
        @author s213555
    */
    public void handleMerchantRegistration(Event e){
        var merchant = e.getArgument(0, MerchantFacade.class);
        var id = registerMerchant(merchant.getFirstName(), merchant.getLastName(), merchant.getCpr(), merchant.getBankAccountId());
        queue.publish(new Event("MerchantRegistered", new Object[] {id}));
    }

    /**
     *
     * @author s194626
     */
    public void handleMerchantDeregister(Event e){
        var merchantId = UUID.fromString(e.getArgument(0, String.class));
        deregisterMerchant(merchantId);
        queue.publish(new Event("MerchantDeregistrationCompleted", new Object[]{merchantId.toString()}));
    }

    /**
     * @author s194626
     */
    private void deregisterMerchant(UUID merchantId) {
        var merchant = merchants.findMerchant(merchantId);
        merchants.removeMerchant(merchant);
    }
}