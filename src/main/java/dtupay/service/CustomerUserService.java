package dtupay.service;

import dtupay.model.Customer;
import dtupay.model.CustomerFacade;
import dtupay.repositories.CustomerRepository;
import messaging.Event;
import messaging.MessageQueue;
import messaging.implementations.RabbitMqQueue;

import java.util.UUID;

public class CustomerUserService {

    private final CustomerRepository customers;
    private final MessageQueue messageQueue;
    
    public CustomerUserService(CustomerRepository customerRepository, RabbitMqQueue mq) {
        this.customers = customerRepository;
        this.messageQueue = mq;
        mq.addHandler("RequestCustomerRegistration",    this::handlerRegisterCustomer);
        mq.addHandler("CustomerDeregistrationRequested",  this::handlerDeregisterCustomer);
        mq.addHandler("CustomerAccountIdRequested",     this::handlerFindBankAccountId);
    }
    /**
        @author s194626
     */
    public UUID registerCustomer(String firstName, String lastName, String cpr, String bankAccountId) {
        Customer customer = new Customer(firstName, lastName, cpr, bankAccountId);
        customers.add(customer);
        return customer.getID();
    }
    /**
        @author s194626
    */
    public String getBankAccountIdOfCustomer(UUID customerId){
        var customer = customers.findCustomer(customerId);
        return customer.getAccountId();
    }
    /**
        @author s194626
    */
    private void handlerRegisterCustomer(Event event) {
        var customer = event.getArgument(0, CustomerFacade.class);
        var id = registerCustomer(customer.getFirstName(), customer.getLastName(),
                customer.getCpr(), customer.getBankAccountId());
        messageQueue.publish(new Event("CustomerRegistered", new Object[]{id}));
    }
    /**
        @author s202082
    */

    private void handlerDeregisterCustomer(Event event) {
        String customerId = event.getArgument(0, String.class);
        var customer = customers.findCustomer(UUID.fromString(customerId));
        customers.removeCustomer(customer);
        messageQueue.publish(new Event("CustomerDeregistrationCompleted", new Object[]{customerId}));
    }
    /**
        @author s194626
    */
    private void handlerFindBankAccountId(Event event) {
        var id = event.getArgument(0, UUID.class);
        var account = getBankAccountIdOfCustomer(id);
        messageQueue.publish(new Event("CustomerAccountIdResult", new Object[]{account}));
    }

}
