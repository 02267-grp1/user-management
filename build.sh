#!/bin/bash
set -e

# Define the service name
Service="user-service"

echo "Building Maven project for
$Service..."
mvn clean install

echo "Building Docker image for $Service..."

docker build . -t $Service 
echo "Docker image for $Service built successfully."
