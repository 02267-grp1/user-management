Feature: Merchant registration feature

  Scenario: Merchant registration
    Given there is a merchant that has not been registered
    When the "MerchantRegistrationRequested" event is received
    Then the merchant is registered
    And the merchant is stored in the merchant repository
