FROM eclipse-temurin:21-jre-alpine
COPY target/lib /usr/src/lib
COPY target/account-service-1.0.0-SNAPSHOT.jar /usr/src/
WORKDIR /usr/src/
CMD java -Xmx64m -jar account-service-1.0.0-SNAPSHOT.jar
